# 1.0.0 (2019-11-15)


### Bug Fixes

* Fixed setup.cfg by removing quotes from desc ([ae45723](https://gitlab.com/dreamer-labs/libraries/jinja2-context-filters/commit/ae45723))


### Features

* Added vars and global vars + tests 100% ([677ca37](https://gitlab.com/dreamer-labs/libraries/jinja2-context-filters/commit/677ca37))
