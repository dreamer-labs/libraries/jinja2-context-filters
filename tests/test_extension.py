from jinja2 import Environment
import pytest


@pytest.fixture
def jinja2_env():
    return Environment(
        extensions=["jinja2_context_filters.Jinja2ContextExtension"])


def test_extension_loads(jinja2_env):
    assert ("context_info" in jinja2_env.filters
            and "environment_info" in jinja2_env.filters), (
        "Missing filters from Envoronment.filters"
    )


@pytest.mark.parametrize("value,output",
                         [["filters", "environment_info"],
                          ["tests", "sequence"],
                          ["extensions",
                           "jinja2_context_filters.Jinja2ContextExtension"]])
def test_environment_info_filter(value, output, jinja2_env):
    rendered = jinja2_env \
        .from_string("{{ '" + value + "' | environment_info }}").render()
    assert output in rendered


@pytest.mark.parametrize("value,output", [["name", "None"],
                                          ["exported", "x"],
                                          ["blocks", "set()"],
                                          ["vars", "x"],
                                          ["global_vars", "test_global_var"]])
def test_context_info_filter(value, output, jinja2_env):

    rendered = jinja2_env \
        .from_string("{% set x = 1 %}{{ '" + value + "' | context_info }}") \
        .render(test_global_var='hey')

    assert output in rendered


def test_context_info_invalid_value(jinja2_env):

    with pytest.raises(ValueError):
        jinja2_env.from_string("{{'not_valid' | context_info}}") \
            .render(test_global_var='hey')


def test_environment_info_invalid_value(jinja2_env):

    with pytest.raises(ValueError):
        jinja2_env.from_string("{{'not_valid' | environment_info}}") \
            .render(test_global_var='hey')
